$(function() {
	var heartTimer;
	var theme = false;

	function darkTheme(enable) {
		if(enable) {
			$('.main-container').css('background-image', 'url("assets/images/bg-night.jpg")');
			$('.infos').addClass('dark');
			$('.info').addClass('dark');
			$('.application').addClass('dark');
		} else {
			$('.main-container').css('background-image', 'url("assets/images/bg.jpg")');
			$('.infos').removeClass('dark');
			$('.info').removeClass('dark');
			$('.application').removeClass('dark');
		}
	}

	$(document).ready(function() {
		setTimeout(function() {
			$('.preloader').fadeOut(250);
		}, 100);
		
		$('.menu').css({
			'opacity': 1,
			'transform': 'scale(1)'
		});

		updatePanel(true);
		setInterval(function() {
			updatePanel(false);
			$.ajax({
				url: 'php/getBPM.php',
				type: 'GET'
			}); 
		}, 2000);

		$('.switch-theme').click(function() {
			theme = !theme;
			var classAdd = theme ? "fas fa-sun" : "fas fa-moon";
			$('.switch-theme i').removeClass();
			$('.switch-theme i').addClass(classAdd);
			darkTheme(theme);
		});
	});

	function updatePanel(animate) {
		$.ajax({
			url: 'php/server.php?q=meteo',
			type: 'GET',
			success: function(response) {
				var meteo = JSON.parse(JSON.parse(response).feedback);
				var $infos = $('.infos[data-type=environment]');
				$infos.find('.temperature').text((Math.round(meteo.main.temp * 10) / 10));
				$infos.find('.humidity').text(meteo.main.humidity);
				if(animate) {
					$infos.find('span').counterUp({
						time: 450
					});
					$infos.find('.info').css({
						'transform': 'rotate(-20deg) scale(1.2)',
						'opacity': 1
					});
					setTimeout(function() {
						$infos.find('.info').css({
							'transform': 'rotate(0deg) scale(1)'
						});
					}, 300);
					$infos.css({
						'opacity': 1,
						'right': '0'
					});
				}
			}
		});

		$.ajax({
			url: 'php/server.php?q=data',
			type: 'GET',
			success: function(response) {
				var user = JSON.parse(JSON.parse(response).feedback);
				var $infos = $('.infos[data-type=health]');
				$infos.find('.heart-frequency').text(user.heartFrequency);
				$infos.find('.temperature').text(user.temperature);
				if(animate) {
					$infos.find('span').counterUp({
						time: 450
					});
					$infos.find('.info').css({
						'transform': 'rotate(-20deg) scale(1.2)',
						'opacity': 1
					});
					setTimeout(function() {
						$infos.find('.info').css({
							'transform': 'rotate(0deg) scale(1)'
						});
					}, 300);
					$infos.css({
						'opacity': 1,
						'left': '0'
					});
					
				}
				var period = 60.0 / parseFloat($('.heart-frequency').text())*1000.0;
				clearInterval(heartTimer);
				heartTimer = setInterval(function() {
					$('.infos .fa-heartbeat').css({
						'top': '-5px',
						'transform': 'scale(1.1)'
					});
					setTimeout(function() {
						$('.infos .fa-heartbeat').css({
							'top': 0,
							'transform': 'scale(1)'
						});
					}, 100);
				}, period);
			}
		});
	}
});