$(function() {
	$(document).ready(function() {
		$('.form-login').submit(function(e) {
			console.log("submit");
			e.preventDefault();
			var formData = new FormData();
			formData.append('email', $('input[name=email]').val());
			formData.append('pass', $('input[name=pass]').val());
			
			$.ajax({
				url: '../php/server.php?q=login',
				type: 'POST',
				data: formData,
				processData: false,
				contentType: false,
				success: function(response) {
					var response = JSON.parse(response);
					if(response.feedback == "OK" && !response.errors.length) {
						document.location = '..';
					} else {
						response.errors.forEach(function(error) {
							$('.errors').html("").append('<p>'+error+'</p>');
						});
					}
				}
			});
		});
	});
});