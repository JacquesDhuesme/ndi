<?php
class MeteoHandler {
	static $key = "1654bb88d185a4d5dc78f8dc62485f93";

	public static function getData($longitude, $latitude) {
		$url = "http://api.openweathermap.org/data/2.5/weather?lat=".doubleval($latitude)."&lon=".doubleval($longitude)."&units=metric&APPID=".self::$key;
		return file_get_contents($url);
	}
}
?>