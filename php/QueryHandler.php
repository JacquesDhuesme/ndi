<?php

class QueryHandler extends Handler {
	public function process(Query $query) {
		$queryName = $query->getQuery();
		if($queryName === "test") {
			$query->setFeedback("1337");
			$query->addError("Mots de passes différents");
			echo $query->serialize();
		} else if($queryName === "data") {
			$healthHandler = new HealthHandler($this->getDb());
			$health = new Health($healthHandler->get(1));
			$query->setFeedback(json_encode($health->toArray()));
			$query->send();
		} else if($queryName === "meteo") {
			$query->setFeedback(MeteoHandler::getData(24.2848041, 15.2614275));
			$query->send();
		} else if($queryName === "login") {
			$email = $this->sanitize($_POST['email']);
			$pass = $this->sanitize($_POST['pass']);
			if($email && $pass) {
				$userHandler = new UserHandler($this->getDb());
				$userHandler->get($email, $pass);
				$auth = isset($_SESSION['authenticated']) ? $_SESSION['authenticated'] : NULL;
				if($auth) {
					$query->setFeedback("OK");
				} else {
					$query->addError("Identifiants incorrects");
				}
			} else {
				$query->addError("Formulaire incomplet.");
			}
			$query->send();
		}
	}

	public function sanitize($var) {
		return isset($var) ? htmlentities($var) : NULL;
	}
}

?>