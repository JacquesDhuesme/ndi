<?php

class Health {
	private $_heartFrequency;
	private $_temperature;
	private $_tension;
	private $_hydratation;

	public function __construct(array $data) {
		foreach($data as $method => $value) {
			$method = "set".ucfirst(strtolower($method));
			if(method_exists($this, $method)) $this->$method($value);
		}
	}

	public function getHeartFrequency() {
		return $this->_heartFrequency;
	}

	public function setHeartFrequency($heartFrequency) {
		$this->_heartFrequency = $heartFrequency;
	}

	public function getTemperature() {
		return $this->_temperature;
	}

	public function setTemperature($temperature) {
		if(is_numeric($temperature)) $this->_temperature = $temperature;
	}

	public function getTension() {
		return $this->_tension;
	}

	public function setTension($tension) {
		if(is_numeric($tension)) $this->_tension = $tension;
	}

	public function getHydratation() {
		return $this->_hydratation;
	}

	public function setHydratation($hydratation) {
		if(is_numeric($hydratation)) $this->_hydratation = $hydratation; 
	}

	public function toArray() {
		return array(
			"heartFrequency" => $this->getHeartFrequency(),
			"temperature" => $this->getTemperature(),
			"tension" => $this->getTension(),
			"hydratation" => $this->getHydratation()
		);
	}
}