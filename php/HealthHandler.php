<?php

class HealthHandler extends Handler {
	public function get($id) {
		$getReq = $this->getDb()->prepare('SELECT FrequenceCardiaque AS heartFrequency, TemperatureCorps AS temperature, Tension, Hydratation FROM sante WHERE Id_sante = :id');
		$getReq->execute(array(
			"id" => $id
		));
		$health = $getReq->fetch();
		$getReq->closeCursor();
		return $health;
	}
}

?>