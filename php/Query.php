<?php
class Query {
	private $_query;
	private $_response;

	public function __construct($query) {
		$this->_query = $query;
		$this->_response = (array(
			"feedback" => "Default feedback message",
			"errors" => array()
		));
	}

	public function getQuery() {
		return $this->_query;
	}

	public function setQuery($query) {
		if(is_string($query)) $this->_query = $query;
	}

	public function getResponse() {
		return $this->_response;
	}

	public function setResponse(array $response) {
		if(!empty($response)) $this->_response = $response;
	}

	public function getFeedback() {
		return $this->getResponse()['feedback'];
	}

	public function setFeedback($feedback) {
		$response = $this->getResponse();
		$response['feedback'] = $feedback;
		$this->setResponse($response);
	}

	public function getErrors() {
		return $this->getResponse()['errors'];
	}

	public function addError($error) {
		$errors = $this->getErrors();
		array_push($errors, $error);
		$this->setErrors($errors);
	}

	public function setErrors(array $errors) {
		$response = $this->getResponse();
		$response['errors'] = $errors;
		$this->setResponse($response);
	}

	public function serialize() {
		return json_encode($this->getResponse());
	}

	public function send() {
		echo $this->serialize();
	}
}