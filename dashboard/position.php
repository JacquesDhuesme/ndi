<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <title>Position et GPS</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <body>
    <?php
        if(!isset($_SESSION['mail'])){
            header("Location: login.php");
            exit;
        }
    ?>
    <div class="w3-sidebar w3-bar-block w3-card" style="width:25%">
            <h3 class="w3-bar-item">Menu</h3>
            <a href="agenda.php" class="w3-bar-item w3-button">Agenda</a>
            <a href="inventaire.php" class="w3-bar-item w3-button">Inventaire</a>
            <a href="meteo.php" class="w3-bar-item w3-button">Meteo</a>
            <a href="position.php" class="w3-bar-item w3-button">Position et GPS</a>
            <a href="sante.php" class="w3-bar-item w3-button">Sante</a>
            <a href="calendrier.php" class="w3-bar-item w3-button">Calendrier</a>
            <a href="journal.php" class="w3-bar-item w3-button">Journal de bord</a>
            <a href="boite.php" class="w3-bar-item w3-button">Boite noir</a>
        </div>
    </div>

    <div style="margin-left:25%">   
        <div class="w3-container w3-teal">
            <h1>Position et GPS</h1>
        </div>
    </div> 
    </body>
</html>