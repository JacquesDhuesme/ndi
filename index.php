<?php

?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/main.css">

    <title>Nuit de l'info</title>
</head>
<body>
    <div class="preloader">
        <div class="loading">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
    <div class="main-container">
        <div class="container-fluid">
            <div class="infos" data-type="environment">
                <div class="info">
                    <p><span class="temperature">0.0</span> °C</p>
                    <i class="fas fa-thermometer-half"></i>
                </div>

                <div class="info">
                    <p><span class="humidity"></span> %</p>
                    <i class="fas fa-tint"></i>
                </div>
            </div>

            <div class="infos" data-type="health">
                <div class="info">
                    <p><span class="heart-frequency">0.0</span></p>
                   <i class="fas fa-heartbeat"></i>
                </div>

                <div class="info">
                    <p><span class="temperature"></span> °C</p>
                    <i class="fas fa-thermometer-three-quarters"></i>
                </div>
            </div>

            <div class="container menu">
                <div class="applications">
                    <a href="dashboard/" class="col-md-3 application">
                        <i class="fas fa-tachometer-alt"></i>
                    </a>
                    <a href="/Chatbot" class="col-md-3 application" >
                        <i class="fas fa-robot"></i>
                    </a>
                    <a href="https://www.facebook.com/FrancsCodeurs" target="_blank" class="col-md-3 application">
                        <i class="fab fa-facebook-square"></i>
                    </a>
                    <a href="#errors" class="col-md-3 application">
                        <i class="fas fa-times"></i>
                    </a>

                    <a href="#errors" class="col-md-3 application switch-theme">
                        <i class="fas fa-moon"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/main.js"></script>
</body>
</html>